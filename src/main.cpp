#include <iostream>
#include <vector>
#include <chrono>
#include <fstream>

using namespace std;

typedef uint64_t nodeKey_t;

class Graph {
    vector<vector<nodeKey_t>> adjList;

    void dfsExplorePost(bool *visited, nodeKey_t *post, nodeKey_t i, nodeKey_t &clock) const
    {
        visited[i] = true;

        for (nodeKey_t in: this->getAllNodesConnectedFrom(i))
            if (!visited[in])
                this->dfsExplorePost(visited, post, in, clock);


        post[clock++] = i;
    }

    void dfsGraphPost(nodeKey_t *post) const {
        auto nc = this->getNodeCount();

        nodeKey_t clock = 0;

        //init visited to false.
        bool *visited = new bool[nc];
        for (size_t i = 0; i < nc; i++)
            visited[i] = false;

        for (nodeKey_t i = 0; i < nc; i++)
            if (!visited[i])
                this->dfsExplorePost(visited, post, i, clock);

        delete[] visited;
    }

    void dfsExploreLabel(bool *visited, std::vector<nodeKey_t> &result, nodeKey_t i) const {
        visited[i] = true;

        for (nodeKey_t in: this->getAllNodesConnectedFrom(i))
            if (!visited[in])
                this->dfsExploreLabel(visited, result, in);

        result.push_back(i);
    }

    void dfsGraphLabel(nodeKey_t const *post, std::vector<std::vector<nodeKey_t>> &result) const {
        auto nc = this->getNodeCount();

        //init visited to false.
        bool *visited = new bool[nc];
        for (size_t i = 0; i < nc; i++)
            visited[i] = false;

        for (nodeKey_t i = nc; i-- > 0;) {
            auto nodeKey = post[i];
            if (!visited[nodeKey]) {
                auto innerResult = vector<nodeKey_t>();
                this->dfsExploreLabel(visited, innerResult, nodeKey);
                result.push_back(innerResult);
            }
        }

        delete[] visited;
    }


public:
    static Graph* generateRandomGraph(int n, int m)
    {
        auto adjM = vector<vector<nodeKey_t>>(n, vector<nodeKey_t>());

        for(int i = 0; i < m; i++)
        {
            nodeKey_t from = rand() % n;
            nodeKey_t to = rand() % n;
            adjM[from].push_back(to);
        }

        return new Graph(adjM);
    }

    static Graph *readGraphFromFile(std::string const &filename) {
        vector<vector<nodeKey_t>> adjMatrix;

        ifstream file(filename);

        if (!file.is_open()) {
            cout << "Failas neegzistuoja" << endl;
            return nullptr;
        }

        size_t nc;
        file >> nc;

        for (size_t i = 0; i < nc; i++) {
            size_t nci;
            file >> nci;

            vector<nodeKey_t> nodes;

            for (size_t j = 0; j < nci; j++) {
                nodeKey_t ni;
                file >> ni;

                nodes.push_back(ni);
            }

            adjMatrix.push_back(nodes);
        }

        file.close();

        return new Graph(adjMatrix);
    }

    Graph() {}
    Graph(vector<vector<nodeKey_t>> const &adjList) : adjList(adjList) {}

    vector<nodeKey_t> const &getAllNodesConnectedFrom(nodeKey_t from) const {
        return adjList.at(from);
    }

    const vector<vector<nodeKey_t>> &getAdjList() const {
        return adjList;
    }

    size_t getNodeCount() const {
        return this->adjList.size();
    }

    Graph *reverse() const {
        auto nc = this->getNodeCount();

        vector<vector<nodeKey_t>> adjListResult = vector<vector<nodeKey_t>>(nc, vector<nodeKey_t>());
        for (nodeKey_t from = 0; from < nc; from++)
            for (nodeKey_t to: this->adjList[from])
                adjListResult[to].push_back(from);

        return new Graph(adjListResult);
    }

    std::vector<std::vector<nodeKey_t>> findStronglyConnectedComponentsInGraph() const {
        auto result = std::vector<std::vector<nodeKey_t>>();
        auto nc = this->getNodeCount();

        //cout << "Reversing graph..." << endl;
        auto reversed = this->reverse();

        //cout << "Running DFS on reversed graph: " << endl;
        auto post = new nodeKey_t[nc];
        reversed->dfsGraphPost(post);

        //for (size_t i = nc; i-- > 0;)
        //    cout << post[i] << endl;

        this->dfsGraphLabel(post, result);

        delete[] post;
        delete reversed;

        return result;
    }

    void print() const {
        auto result = this->getAdjList();
        auto nc = this->getNodeCount();
        if (result.empty()) {
            cout << "Tuščias grafas" << endl;
            return;
        }

        cout << "Grafas, su " << nc << " viršūnių: " << endl;
        for (nodeKey_t from = 0; from < nc; from++) {
            cout << "{" << from << "} -> [ ";

            for (auto to: result[from])
                cout << to << " ";

            cout << "]" << endl;
        }
    }
};

void printResult(std::vector<std::vector<nodeKey_t>> const &result) {
    if (result.empty()) {
        cout << "Stipriai jungiu komponenčių duotame grafe (Rezultatas): Nėra" << endl;
        return;
    }

    cout << "Stipriai jungios komponentės duotame grafe (Rezultatas): " << endl;
    for (auto const &component: result) {
        cout << "\t[ ";
        for (auto nodeKey: component)
            cout << nodeKey << " ";
        cout << "]" << endl;
    }
}

void printHelp() {
    cout << endl
         << "Naudojimas: ./main {failas}, kur:" << endl
         << " * failas - kelias iki failo, iš kurio nuskaityti grafą" << endl;
}

void runTest(int n, int m)
{
    cout << "Testuojame, kai n = " << n << ", m = " << m << ", n + m = " << n + m << endl;
    auto graph = Graph::generateRandomGraph(n, m);

    auto startTime = chrono::high_resolution_clock::now();
    auto result = graph->findStronglyConnectedComponentsInGraph();

    auto endTime = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(endTime - startTime);

    cout << "Rezultatas: " << duration.count() << " ms." << endl;

    delete graph;
}

int runTests() {

    cout << "Testai nuo m:" << endl;
    runTest(10000, 1);
    runTest(10000, 10);
    runTest(10000, 100);
    runTest(10000, 1000);
    runTest(10000, 10000);
    runTest(10000, 100000);
    runTest(10000, 1000000);

    cout << "Testai nuo n:" << endl;
    runTest(1, 10000);
    runTest(10, 10000);
    runTest(100, 10000);
    runTest(1000, 10000);
    runTest(10000, 10000);
    runTest(100000, 10000);
    runTest(1000000, 10000);
}

int main(int argc, const char **argv) {
    srand(time(NULL));

    cout << "Algoritmu analizės projektas." << endl;

    if (argc != 2) {
        printHelp();
        return -1;
    }

    string fileName = string(argv[1]);
    if(fileName == "--test")
        return runTests();

    cout << "Nuskaitomas grafas iš " << fileName << "..." << endl;
    Graph *graph = Graph::readGraphFromFile(fileName);

    if (graph == nullptr)
        return -1;

    graph->print();

    cout << "Paimamas startTime laikas..." << endl;
    cout << "Ieškomos stiprios komponentės..." << endl;
    auto startTime = chrono::high_resolution_clock::now();
    auto result = graph->findStronglyConnectedComponentsInGraph();

    auto endTime = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::nanoseconds>(endTime - startTime);

    printResult(result);
    cout << "Rezultatas rastas per " << duration.count() << " ns." << endl;

    delete graph;

    return 0;
}
